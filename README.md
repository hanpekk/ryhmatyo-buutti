# Purple Team - Group Project

Our awesome gaming desktop application made as practice in CodeMatch Academy at Buutti Education

- Totally free registration 
- Synchronise with your Steam account
- Launch your Steam games
- Keep track of all your Steam games
- Manage your account with the React app

(We are not affiliated with Valve in anyway nor was Valve included in this excercise. Steam is property of Valve)

## Installation

This app uses C++ and Qt for ui/frontend, C# for server/backend and PostgreSQL for database.
To run it you need:
- Qt Creator to run frontend (preferred)
- e. g. Visual Studio Code or equivalent to run the C# server
- e. g. PgAdmin 4 to handle the database

Server/Backend code at https://gitlab.com/Miketti/ryhmatyo-buutti-server .
Alternate React frontend at https://gitlab.com/Miketti/ryhmatyo-buutti-react. 

## Authors

Hannu-Pekka Kangas (C++/Qt frontend)
Mika Korkiasaari @miketti (server, database, React frontend)
Carita Shemeikka @caritaemilia (server, database)

