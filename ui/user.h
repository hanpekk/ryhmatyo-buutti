#ifndef USER_H
#define USER_H

#include <QObject>

class User : public QObject
{
    Q_OBJECT

public:
    explicit User(QObject *parent = nullptr);

   static QString email;
   static QString username;
   static QString password;
   static QString verify;
   static QString token;
   static QString newPassword;
   static QString code;
   static int id;
   static QString steamId;
   static QString steamPersona;

signals:

};

#endif // USER_H
