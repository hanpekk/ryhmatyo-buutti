#ifndef REGISTER_H
#define REGISTER_H

#include <QDialog>
#include "client.h"
#include "user.h"

namespace Ui {
class Register;
}

class Register : public QDialog
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = nullptr);
    ~Register();

    Client *client;

private slots:
    void on_buttonCreate_clicked();
    void validate();
    bool validateEmail();
    bool validateUsername();
    bool validatePassword();
    bool validateVerify();
    void result();
    void on_buttonBacktoLogin_clicked();

public slots:
    void disableAll();
    void enableAll();
    void clearAll();

private:
    Ui::Register *ui;

};

#endif // REGISTER_H
