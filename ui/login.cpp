#include "login.h"
#include "ui_login.h"
#include "constants.h"
#include <QTimer>
#include "passwordwizard.h"
#include "ui_custommodal.h"

using namespace Constants;

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui -> setupUi(this);
    client = new Client (this);
    registerW = new Register(this);
    verifyModal = new CustomModal();
    QObject::connect(client -> manager, &QNetworkAccessManager::finished, this, &Login::result);
    QObject::connect(ui -> textEmail, &QLineEdit::textEdited, this, &Login::validateEmail);
    QObject::connect(ui -> textEmail, &QLineEdit::textEdited, this, &Login::validate);
    QObject::connect(ui -> textPassword, &QLineEdit::textEdited, this, &Login::validatePassword);
    QObject::connect(ui -> textPassword, &QLineEdit::textEdited, this, &Login::validate);
    QObject::connect(verifyModal->ui->textEdit, &QLineEdit::textEdited, this, &Login::validateVerify);
    ui -> buttonLogin -> setEnabled(false);
}

Login::~Login()
{
    delete ui;
}

void Login::on_buttonLogin_clicked()
{
    disableAll();
    ui -> status -> clear();
    client -> user -> email =  ui -> textEmail -> text();
    client -> user -> password = ui -> textPassword -> text();
    client -> postLogin(client -> user);
}

void Login::result()
{

    switch (client -> statusCode) {
    case(Client::noConnection):
        ui -> status -> setText("Could not establish connection to server");
        enableAll();
        break;
    case(Client::badRequest):
        ui -> status -> setText("Invalid username, password or verification code");
        enableAll();
        break;
    case(Client::statusOk):
        disableAll();
        ui -> status -> setText("Logged in successfully!");
        client -> getUserData();
        emit loggedIn();
        QTimer::singleShot(2000, this,  SLOT(Accept()));
        break;
    case(Client::userNotVerified):
        verifyModal -> setWindowTitle("Account verification");
        verifyModal -> hideOkButton();
        verifyModal -> ui -> buttonBox -> button(QDialogButtonBox::Ok) -> setText("Send");
        enableAll();
        ui -> status -> setText("User has not been verified");
        verifyModal -> setLabel("User has not been verified. Type in the verification code that was sent to your email address.");
        if(verifyModal -> exec() == QDialog::Accepted)
        {
            client -> user -> code = verifyModal -> returnTextFromTextEdit();
            client -> verifyUser(client -> user);
        }
        break;
    }
}
void Login::validateVerify()
{
    QRegularExpression regex("^[a-zA-Z0-9]+$");
    if(regex.match(verifyModal->returnTextFromTextEdit()).hasMatch())
    {
        verifyModal->enableOk();
    }
    else
    {
        verifyModal->disableOk();
    }
}

bool Login::validateEmail()
{
    bool valid = false;
    client -> user -> email =  ui -> textEmail -> text();
    QRegularExpression regex("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
    if(regex.match(client->user->email).hasMatch()){
        valid = true;
        ui -> textEmail -> setStyleSheet("QLineEdit#textEmail { border: 1px solid green }");
    }else{
        ui -> textEmail -> setStyleSheet("QLineEdit#textEmail { border: 1px solid red }");
    }
    return valid;
}

bool Login::validatePassword()
{
    bool valid = false;
    client -> user -> password = ui -> textPassword -> text();
    if(client -> user -> password.length() >= g_MinPasswordLen){
        valid = true;
        ui -> textPassword -> setStyleSheet("QLineEdit#textPassword { border: 1px solid green }");
    }else{
        ui -> textPassword -> setStyleSheet("QLineEdit#textPassword{ border: 1px solid red }");
    }
    return valid;
}

void Login::validate()
{
    ui -> buttonLogin -> setEnabled(false);
    if(validateEmail() == true
            &&validatePassword() == true){
        ui -> buttonLogin -> setEnabled(true);
    }
}

void Login::on_buttonCancel_clicked()
{
    this -> reject();
}

void Login::on_buttonCreateAccount_clicked()
{
    this -> hide();
    registerW -> exec();
    registerW -> clearAll();
    registerW -> enableAll();
    this -> show();

}

void Login::Accept(){
    this -> accept();
}

void Login::disableAll()
{
    ui -> buttonLogin -> setEnabled(false);
    ui -> buttonCreateAccount -> setEnabled(false);
    ui -> buttonCancel -> setEnabled(false);
    ui -> textEmail -> setEnabled(false);
    ui -> textPassword -> setEnabled(false);
    ui -> buttonForgotPassword -> setEnabled(false);
}

void Login::enableAll()
{
    ui -> buttonLogin -> setEnabled(true);
    ui -> buttonCreateAccount -> setEnabled(true);
    ui -> buttonCancel -> setEnabled(true);
    ui -> textEmail -> setEnabled(true);
    ui -> textPassword -> setEnabled(true);
    ui -> buttonForgotPassword -> setEnabled(true);
}

void Login::on_buttonForgotPassword_clicked()
{
    PasswordWizard wiz;
    wiz.exec();
}
