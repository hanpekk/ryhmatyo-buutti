#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QtDebug>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QHostAddress>
#include <QIODevice>
#include <QUrl>
#include <QJsonArray>
#include <QPixmap>

#include "constants.h"
#include "jsonhandler.h"
#include "user.h"

class OriginatingObject : public QObject
{
    Q_OBJECT
public :
    OriginatingObject(QObject *parent = nullptr);
};

class Client : public QObject
{
    Q_OBJECT

public:
    explicit Client(QObject *parent = nullptr);

    enum ErrorCodes
    {
        statusOk= 200,
        badRequest = 400,
        noConnection = 0,
        userNotVerified = 401,
    };

    int statusCode;

    QString error;
    QString text;
    const QString STATUS_MSG_PASSWORD_CHANGE_SUCCESSFUL = "Password changed successfully!";
    const QString STATUS_MSG_LOGIN_SUCCESSFUL = "Logged in successfully!";
    const QString STATUS_MSG_NO_CONNECTION = "Could not establish connection to server";
    const QString STATUS_MSG_INVALID_EMAIL = "Invalid email address";
    const QString STATUS_MSG_INVALID_CODE = "Invalid verification code";
    const QString STATUS_MSG_CODE_SUCCESSFUL = "Code was successfully sent to email address";

    const QString serverAddress = "http://127.0.0.1:5000/api/";
    const QUrl urlGetUsers = QUrl(serverAddress + "users");
    QUrl urlPostLogin = QUrl(serverAddress + "login");
    QUrl urlPostRegister = QUrl(serverAddress + "register");
    QUrl urlPostForgottenPassword = QUrl(serverAddress + "users/forgottenpassword");
    QUrl urlPatchResetPassword = QUrl(serverAddress + "users/resetpassword");
    QUrl urlPatchVerifyUser = QUrl(serverAddress + "users/verify");

    OriginatingObject *ooGetOwnedGames;
    OriginatingObject *ooDownloadImage;

    QVector <QString> steamIdPaths;
    QString steamIdFile = "loginusers.vdf";

    QNetworkReply *reply;
    QNetworkAccessManager *manager;
    QPixmap *image;

    User *user;

public slots:

    void authenticate(QNetworkRequest *request);
    void httpReadyRead(QNetworkReply *reply);
    void getUsers();

    void addSteamGamesToDatabase(User *user);
    void getUserSteamGames(User *user);
    void addExtraSteamGameDataToDatabase(int appid);
    void getIndividualSteamGameData(int appid);

    void postLogin(User *user);
    void postRegister(User *user);
    void changePassword(User *user);
    void forgottenPassword(User *user);
    void resetPassword(User *user);
    void getUserData();
    void verifyUser(User *user);

    void addFriend(User *user, QString friendName);
    void getFriends();
    void removeFriend(User *user, QString friendName);

    void getSteamId();
    void downloadSteamImage(QString url);
    void sendSteamId();

};

#endif // CLIENT_H
