#ifndef ALLGAMES_H
#define ALLGAMES_H

#include <QWidget>
#include <QTableWidget>
#include "gametab.h"

class AllGames : public GameTab
{
    Q_OBJECT

public:
    explicit AllGames(QWidget *parent = nullptr);
    ~AllGames();

public slots:

   void setupTable();
   void on_refresh_clicked();

};

#endif // ALLGAMES_H
