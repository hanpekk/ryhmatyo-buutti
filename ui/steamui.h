#ifndef STEAMUI_H
#define STEAMUI_H

#include <QWidget>
#include <QTableWidgetItem>
#include <QFileSystemWatcher>
#include "gametab.h"
#include "client.h"
#include "steamgame.h"

class SteamUi : public GameTab
{
    Q_OBJECT
public:
    explicit SteamUi(QWidget *parent = nullptr);
    ~SteamUi();

public slots:

    void findSteamImageFolderPath();
    void setupTable();
    void populateGames();
    void on_updateTable_clicked();
    SteamGame* createSteamGame(QString name, int appid, int playtime, int releaseYear, QList<QString> publishers , QList<QString> developers, QList<QString> genres);

};

#endif // STEAMUI_H
