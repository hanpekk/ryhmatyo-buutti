#include "jsonhandler.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QtGlobal>
#include <QString>
#include <QIODevice>

JsonHandler::JsonHandler(QObject *parent) : QObject(parent)
{}

QString JsonHandler::readJson(QString fileName)
{
    QString value ;
    QFile file(fileName);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    value = file.readAll();
    file.close();
    return value;
}

void JsonHandler::writeToJson(QString fileName, QString contents)
{
    QFile file(fileName);
    QTextStream stream(&file);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    stream<<contents;
    file.close();
}

void JsonHandler::appendToJson(QString fileName, QString contents)
{
    QFile file(fileName);
    QTextStream stream(&file);
    file.open(QIODevice::Append | QIODevice::Text);
    stream<<contents;
    file.close();
}

QVariant JsonHandler::getJsonValueFromString(QString jsonData, QString key)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();
    QJsonValue jsonValue = jsonObj[key];
    return jsonValue;
}

QVariant JsonHandler::getJsonValueFromNestedJson(QString jsonData, QString key1, QString key2)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();
    jsonObj = jsonObj[key1].toObject();
    QJsonValue jsonValue = jsonObj[key2];
    return jsonValue;
}

QVariant JsonHandler::getJsonValueFromSteamPlayerSummary(QString jsonData, QString key)
{
    QFile file(jsonData);
    QString text ;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    text = file.readAll();
    file.close();

    QJsonDocument jsonText = QJsonDocument::fromJson(text.toUtf8());
    QJsonObject jsonObj = jsonText.object();
    jsonObj = jsonObj["response"].toObject();
    QJsonValue jsonValue = jsonObj["players"];
    QJsonArray array = jsonValue.toArray();
    QVariant data;
    for (QJsonValue value : array)
    {
        data = value[key];
    }
    return data;
}

QVariant JsonHandler::getJsonValueFromSteamOwnedGames(QString jsonData, QString key)
{
    QFile file(jsonData);
    QString text ;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    text = file.readAll();
    file.close();

    QJsonDocument jsonText = QJsonDocument::fromJson(text.toUtf8());
    QJsonObject jasonObj = jsonText.object();
    QJsonValue jsonValue = jasonObj.value("games");
    QJsonArray array = jsonValue.toArray();
    QVariant data;
    for (QJsonValue  value : array)
    {
        data = value[key];
    }
    return data;
}

