#include "gametab.h"
#include "custommodal.h"
#include "ui_gametab.h"
#include <QDir>
#include <QProcess>
QVector <SteamGame*> GameTab::steamGames;

GameTab::GameTab(QWidget *parent) : QWidget(parent),ui(new Ui::GameTab)
{
    ui -> setupUi(this);
    client = new Client(this);

    homePath = QDir::homePath();

    steamImageFolderPaths
            << homePath + "/.steam/steam/appcache/librarycache/"
            << "C:/Program Files (x86)/Steam/appcache/librarycache/"
            << "C:/Program Files/Steam/appcache/librarycache/";


    steamAppManifestPaths
            << homePath + "/.steam/steam/steamapps/"
            << "C:/Program Files (x86)/Steam/steamapps/"
            << "C:/Program Files/Steam/steamapps/";

    ui->launchGame->setEnabled(false);

}

QTableWidgetItem *GameTab::createQTableWidgetItem(SteamGame game)
{
    QTableWidgetItem *item = new QTableWidgetItem();
    item -> setBackground(game.image);
    return  item;
}

QTableWidgetItem *GameTab::createQTableWidgetItem(QString name)
{
    QTableWidgetItem *item = new QTableWidgetItem();
    item -> setText(name);
    return  item;
}

void GameTab::populateItemTable()
{
    for(auto game : steamGames)
    {
        steamGameBanners << createQTableWidgetItem( *game);
        steamGameNames << createQTableWidgetItem(game -> name);
    }
}

void GameTab::setGameTable()
{
    ui -> gamesTableWidget -> setColumnCount(columnsAmount);
    ui -> gamesTableWidget -> setRowCount(steamGames.size());
    ui -> gamesTableWidget -> verticalHeader() -> setVisible(false);
    int rowCounter = 0;
    for(auto item : steamGameBanners)
    {
        item -> setFlags(item -> flags() &  ~Qt::ItemIsEditable);
        ui -> gamesTableWidget -> setItem(rowCounter, BANNER, item);
        ui -> gamesTableWidget -> setColumnWidth(BANNER, steamBannerWidth );
        ui -> gamesTableWidget -> setRowHeight(item -> row(), steamBannerHeigth);
        rowCounter++;
    }
    rowCounter = 0;
    for (auto item : steamGameNames )
    {
        item -> setFlags(item -> flags() &  ~Qt::ItemIsEditable);
        ui -> gamesTableWidget -> setItem(rowCounter, NAME, item);
        ui -> gamesTableWidget -> resizeColumnToContents(item -> column());

        rowCounter++;
    }
    QStringList headers;
    headers<<""<<"Name";
    ui -> gamesTableWidget -> setHorizontalHeaderLabels(headers);
    ui -> gamesTableWidget -> setSelectionBehavior(QAbstractItemView::SelectRows);
    ui -> gamesTableWidget -> setSelectionMode(QAbstractItemView::SingleSelection);
    ui -> gamesTableWidget -> sortByColumn(NAME,Qt::AscendingOrder);

    QObject::connect(ui -> gamesTableWidget,&QTableWidget::currentItemChanged,[this](QTableWidgetItem *item){
        ui->launchGame->setEnabled(true);
        nameOfGameClicked = item -> data(Qt::DisplayRole).toString();
        showGameDetails(item -> data(Qt::DisplayRole).toString());
        for(auto &game : steamGames)
        {
            if(game->name == item->data(Qt::DisplayRole).toString())
            {
                /* Crude approach. Requires writing temp file and refreshing the table to show data*/
                client->addExtraSteamGameDataToDatabase(game->appid);
                break;
            }
        }

    });
}

void GameTab::showGameDetails(QString name)
{
    /* TODO Show played time */
    for (auto game  : steamGames) {
        if(game -> name == name){
            ui -> picture -> setPixmap(game -> libraryImage);
            ui -> name -> setText(game -> name);
            ui -> year -> setText("Year: " + QString::number(game -> releaseYear));
            QString devs, pubs, gens;
            for (int i = 0 ; i < game -> publishers.size() ; i++ )
            {
                pubs.append(game->publishers.at(i));
                pubs.append(", ");
            }
            ui -> publisher -> setText("Publisher: " + pubs);

             for (int i = 0 ; i < game -> developers.size() ; i++ )
            {
                devs.append(game->developers.at(i));
                devs.append(", ");
            }
            ui -> developer -> setText("Developer: " + devs);

            for (int i = 0 ; i < game -> genres.size() ; i++ )
            {
                gens.append(game->genres.at(i));
                gens.append(", ");
            }
            ui -> genre -> setText("Genre: " + gens);

            break;
        }
    }
}

void GameTab::clearGameDetails()
{
    disconnect(ui -> gamesTableWidget,&QTableWidget::currentItemChanged,nullptr,nullptr);
    ui-> name->clear();
    ui-> picture->clear();
    ui-> name->clear();
    ui-> year->clear();
    ui-> publisher->clear();
    ui-> developer->clear();
    ui-> genre->clear();
}

void GameTab::onTableClicked(const QModelIndex &index)
{
    if (index.isValid()) {
        nameOfGameClicked = index.data().toString();
        ui->launchGame->setEnabled(true);
    }
}

void GameTab::clearGameTable()
{
    ui->gamesTableWidget->hide();
    steamGames.clear();
    steamGameBanners.clear();
    steamGameNames.clear();
    clearGameDetails();
    ui->launchGame->setEnabled(false);
    ui->gamesTableWidget->clearSelection();
    ui->gamesTableWidget->clearContents();
    ui->gamesTableWidget->setRowCount(0);
    ui->gamesTableWidget->show();
}

void GameTab::launchGame()
{
    int appid = 0;
    for(auto &game : steamGames)
    {
        if(game -> name == nameOfGameClicked)
        {
            appid = game -> appid;
            break;
        }
    }
    QString appIdStr;
    appIdStr = appIdStr.number(appid);

    QFile appManifest;
    bool gameNotInstalled = true;
    for (auto &path : steamAppManifestPaths)
    {
        appManifest.setFileName(path + "appmanifest_" + appIdStr + ".acf");
        if(appManifest.exists())
        {
            gameNotInstalled = false;
            break;
        }
    }

    if(gameNotInstalled)
    {
        CustomModal modal;
        modal.setWindowTitle("Game not installed");
        modal.setLabel("The game " + nameOfGameClicked + " is not installed or it was not found in the default paths");
        modal.hideButtonBox();
        modal.hideTextEdit();
        modal.exec();
    }

    else
    {
        QString command;
        if(appManifest.fileName().contains("C:"))
        {
            command = "cmd";
        }

        else
        {
            command = "steam";
        }

        QStringList arguments;
        arguments << "steam://rungameid/" + appIdStr;
        QProcess *game = new QProcess(this);
        game->execute(command,arguments);
        delete game;
    }

}

void GameTab::on_updateTable_clicked()
{}

void GameTab::on_launchGame_clicked()
{
    launchGame();
}

