#ifndef GAMETAB_H
#define GAMETAB_H

#include <QWidget>
#include <QVector>
#include <QTableWidgetItem>
#include <QAction>
#include "steamgame.h"
#include "client.h"
namespace Ui {
class GameTab;
}
class GameTab : public QWidget
{
    Q_OBJECT
public:
    explicit GameTab(QWidget *parent = nullptr);

    Client *client;

    static  QVector<SteamGame*> steamGames;
    QVector<QTableWidgetItem*> steamGameBanners;
    QVector<QTableWidgetItem*> steamGameNames;
    QVector <QString> steamAppManifestPaths;

    QString nameOfGameClicked;

    int steamBannerHeigth = 216;
    int steamBannerWidth = 460;
    int columnsAmount = 2;
    enum Columns {BANNER, NAME};

    QString homePath;
    QVector <QString> steamImageFolderPaths;
    QString steamImageFolderPath;
    QString steamBannerImagePathEnd = "_header.jpg";
    QString steamLibraryImagePathEnd = "_library_600x900.jpg";

    QTableWidgetItem* createQTableWidgetItem(SteamGame game);
    QTableWidgetItem* createQTableWidgetItem(QString name);
    void populateItemTable();
    void setGameTable();
    void clearGameTable();

    Ui::GameTab *ui;

public slots:
    void showGameDetails(QString name);
    void clearGameDetails();
    void launchGame();
    void onTableClicked(const QModelIndex &);
    virtual void on_updateTable_clicked();

    void on_launchGame_clicked();
};

#endif // GAMETAB_H
