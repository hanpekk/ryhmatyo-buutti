#include "userui.h"
#include "ui_userui.h"
#include "constants.h"
#include "jsonhandler.h"
#include "custommodal.h"

using namespace Constants;

UserUi::UserUi(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Userui)
{
    ui -> setupUi(this);
    client = new Client(this);
    avatar = new QPixmap();
    user = new User(this);
    watcher = new QFileSystemWatcher(this);
    watcher -> addPath(g_SteamUserFile);
    QObject::connect(watcher, &QFileSystemWatcher::fileChanged, this, &UserUi::prepareSteamUser);
    QObject::connect(client -> manager, &QNetworkAccessManager::finished, this, &UserUi::showSteamAvatar);
}

UserUi::~UserUi()
{
    delete ui;
}

void UserUi::on_buttonLogout_clicked()
{
    emit logOut();
}

void UserUi::on_buttonChangePsw_clicked()
{
    emit changePsw();
}

void UserUi::on_buttonSyncSteam_clicked()
{
    CustomModal modal;
    modal.setWindowTitle("Steam synchronisation");

    /* TODO:  check if user already has SteamId linked to account, then get games. If not, search SteamId from computer and send it to server and continue
    Requires data from server during login*/

    client -> getSteamId();
    client -> sendSteamId();

    if(!(user -> steamId == ""))
    {
        modal.hideTextEdit();
        modal.hideOkButton();
        modal.setLabel("Found Steam User ID " + user -> steamId + " on system. Synchronise?");
        client -> addSteamGamesToDatabase(user);
        if(modal.exec() == QDialog::Accepted)
        {
            client -> getUserSteamGames(user);
            emit steamSync();
        }
    }
    else
    {
        modal.setLabel("No Steam User ID was found. Start up Steam and log in with your account at least once on this computer");
        modal.hideButtonBox();
        modal.hideTextEdit();
        modal.exec();
    }
}

void UserUi::changeLoggedInAsName()
{
    ui -> labelLoggedInAs -> setText("Logged in as: <b>" + user -> username + "</b>");
}

void UserUi::prepareSteamUser()
{
    /* TODO: Get Steam persona and Steam image from server

    user -> steamPersona = JsonHandler::getJsonValueFromSteamPlayerSummary(g_SteamUserFile, steamNickname).toString();
    client -> downloadSteamImage(JsonHandler::getJsonValueFromSteamPlayerSummary(g_SteamUserFile, steamUserAvatar).toString());
    ui -> labelSteamnick -> setText("Steam persona: <b>" + user -> steamPersona + "</b>");

    */
}

void UserUi::showSteamAvatar()
{
    avatar = client -> image;
    ui -> labelSteamImage -> setPixmap(*avatar);
}
