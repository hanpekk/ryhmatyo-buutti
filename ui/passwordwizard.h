#ifndef PASSWORDWIZARD_H
#define PASSWORDWIZARD_H

#include <QWidget>
#include <QWizard>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QObject>
#include "client.h"

class DataSender : public QObject
{
    Q_OBJECT
public:
    explicit DataSender(QObject *parent = nullptr);

    Client *client;

public slots:
    void sendEmail();
    void sendCodeAndPassword();

};

class PasswordWizard : public QWizard
{
    Q_OBJECT
public:

    enum
    {
        Page_First,
        Page_Last
    };

    explicit PasswordWizard(QWidget *parent = nullptr);

};

class FirstPage : public QWizardPage
{
    Q_OBJECT
public:

    explicit FirstPage(QWidget *parent = nullptr);
    QLabel *label;
    QLineEdit *email;
    QVBoxLayout *layout;
    QPushButton *send;
    QLabel *status;
    bool emailSent = false;
    DataSender *datasender;

public slots:

    void disableAll();
    void setEmail();
    void result();
    bool isComplete() const;

};

class SecondPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit SecondPage(QWidget *parent = nullptr);

    QLineEdit *edit;
    QLabel *label;
    QLabel *labelPassword;
    QLabel *labelCode;
    QLabel *labelVerify;
    QLineEdit *code;
    QLineEdit *password;
    QLineEdit *verify;
    QVBoxLayout *layout;
    QPushButton *send;
    QLabel *status;

    bool codeSent = false;

    DataSender *datasender;
public slots:
    bool isComplete() const;
    void verifyCodeAndPassword();
    void disableAll();
    void enableAll();
    void result();

};

#endif // PASSWORDWIZARD_H
