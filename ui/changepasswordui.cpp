#include "changepasswordui.h"
#include "ui_changepasswordui.h"
#include "constants.h"
#include <QTimer>

using namespace Constants;

Changepasswordui::Changepasswordui(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Changepasswordui)
{
    ui -> setupUi(this);
    client = new Client(this);

    ui -> status -> clear();
    QObject::connect(client -> manager, &QNetworkAccessManager::finished, this, &Changepasswordui::result);
    QObject::connect(ui -> textPassword,&QLineEdit::textEdited,this, &Changepasswordui::validateOldPassword);
    QObject::connect(ui -> textNewPassword,&QLineEdit::textEdited,this, &Changepasswordui::validateNewPassword);
    QObject::connect(ui -> textVerify,&QLineEdit::textEdited,this, &Changepasswordui::validateNewPassword);
    QObject::connect(ui -> textPassword,&QLineEdit::textEdited,this, &Changepasswordui::validate);
    QObject::connect(ui -> textNewPassword,&QLineEdit::textEdited,this, &Changepasswordui::validate);
    QObject::connect(ui -> textVerify,&QLineEdit::textEdited,this, &Changepasswordui::validate);
    ui -> buttonConfirm -> setEnabled(false);
}

Changepasswordui::~Changepasswordui()
{
    delete ui;
}

void Changepasswordui::on_buttonConfirm_clicked()
{
    disableAll();
    ui -> status -> clear();
    client -> user -> password = ui -> textPassword -> text();
    client -> user -> verify =  ui -> textVerify -> text();
    client -> user -> newPassword = ui -> textNewPassword -> text();
    client -> changePassword(client -> user);
}

bool Changepasswordui::validateOldPassword()
{
    bool valid = false;
    if(ui -> textPassword -> text().length() >= g_MinPasswordLen ){
        valid = true;
        ui -> textPassword -> setStyleSheet("QLineEdit#textPassword { border: 1px solid green }");
    }else{
        ui -> textPassword -> setStyleSheet("QLineEdit#textPassword{ border: 1px solid red }");
    }
    return valid;
}

bool Changepasswordui::validateNewPassword()
{
    bool valid = false;
    if(ui -> textNewPassword -> text().length() >= g_MinPasswordLen  ){
        ui -> textNewPassword -> setStyleSheet("QLineEdit#textNewPassword { border: 1px solid green }");
        if(ui -> textVerify -> text()  ==  ui -> textNewPassword -> text()){
            ui -> textVerify -> setStyleSheet("QLineEdit#textVerify { border: 1px solid green }");
            valid = true;
        }
    }
    else{
        ui -> textNewPassword -> setStyleSheet("QLineEdit#textNewPassword{ border: 1px solid red }");
        ui -> textVerify -> setStyleSheet("QLineEdit#textVerify{ border: 1px solid red }");
    }
    return valid;
}

bool Changepasswordui::validate()
{
    ui -> buttonConfirm -> setEnabled(false);
    bool valid = false;
    if(validateOldPassword() == true
            &&validateNewPassword() == true){
        valid = true;
        ui -> buttonConfirm -> setEnabled(true);
    }
    return valid;
}

void Changepasswordui::result()
{
    switch (client  ->  statusCode) {
    case(Client::noConnection):
        enableAll();
        ui -> status -> setText(client -> STATUS_MSG_NO_CONNECTION);
    case(Client::statusOk):
        ui -> status -> setText(client -> STATUS_MSG_PASSWORD_CHANGE_SUCCESSFUL);
        QTimer::singleShot(2000, this,  SLOT(Close()));
    }
}

void Changepasswordui::disableAll()
{
    ui -> textNewPassword -> setEnabled(false);
    ui -> textPassword -> setEnabled(false);
    ui -> textVerify -> setEnabled(false);
    ui -> buttonClose -> setEnabled(false);
    ui -> buttonConfirm -> setEnabled(false);
}

void Changepasswordui::enableAll()
{
    ui -> textNewPassword -> setEnabled(true);
    ui -> textVerify -> setEnabled(true);
    ui -> textPassword -> setEnabled(true);
    ui -> buttonConfirm -> setEnabled(true);
    ui -> buttonClose -> setEnabled(true);
}

void Changepasswordui::Close()
{
    this -> close();
}
