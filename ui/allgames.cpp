#include "allgames.h"

AllGames::AllGames(QWidget *parent) :
    GameTab(parent)
{}

AllGames::~AllGames()
{}

void AllGames::setupTable()
{
    populateItemTable();
    setGameTable();
}

void AllGames::on_refresh_clicked()
{
    setupTable();
}

