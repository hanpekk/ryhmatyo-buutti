#include <QProcess>
#include <QPlainTextEdit>
#include <QObject>
#include <QLabel>
#include <QtNetwork/QNetworkAccessManager>
#include <QLineEdit>
#include "mainwindow.h"
#include "constants.h"

#include "./ui_mainwindow.h"

using namespace Constants;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui -> setupUi(this);
    loginWindow = new Login(this);
    QObject::connect(loginWindow, &Login::rejected, this, &MainWindow::close);

    loginWindow -> open();

    mainWidget = new QWidget;
    setCentralWidget(mainWidget);

    vbox = new QVBoxLayout;
    mainWidget -> setLayout(vbox);

    QTabWidget *viewsTabWidget = new QTabWidget;
    vbox -> addWidget(viewsTabWidget);

    UserUi *userUiForm = createUserTab();
    QWidget *storeWidget = createStoreTab();
    Social *socialUi = createSocialTab();
    QWidget *gamesWidget = createGamesTab();

    viewsTabWidget -> addTab(gamesWidget,"Games");
    viewsTabWidget -> addTab(storeWidget,"Store");
    viewsTabWidget -> addTab(socialUi,"Social");
    viewsTabWidget -> addTab(userUiForm, "User");
}

MainWindow::~MainWindow()
{
    delete ui;
}

QWidget* MainWindow::createGamesTab()
{
    QWidget *gamesViewWidget = new QWidget;
    QHBoxLayout *gamesLayout = new QHBoxLayout;
    gamesViewWidget -> setLayout(gamesLayout);
    verticalTabWidget = new CustomTabWidget(this);
    gamesLayout -> addWidget(verticalTabWidget);


    verticalTabWidget -> addTab(createAllGamesUi(),"All games");
    verticalTabWidget -> addTab(createSteamUi(),"Steam");

    return gamesViewWidget;
}

QWidget *MainWindow::createStoreTab()
{
    QWidget *storeWidget = new QWidget;
    return storeWidget;
}

Social *MainWindow::createSocialTab()
{
    Social *socialUi = new Social(this);
    return socialUi;
}

UserUi *MainWindow::createUserTab()
{
    userUi = new UserUi(this);
    QObject::connect(userUi, &UserUi::changePsw, this, &MainWindow::changePsw);
    QObject::connect(userUi, &UserUi::logOut, this, &MainWindow::logOut);
    QObject::connect(loginWindow, &Login::loggedIn, userUi, &UserUi::changeLoggedInAsName);
    return userUi;
}

SteamUi *MainWindow::createSteamUi()
{
    steamUi = new SteamUi(this);
    return steamUi;
}

AllGames *MainWindow::createAllGamesUi()
{
    allGames = new AllGames(this);
    allGames -> setupTable();
    return allGames;
}

void MainWindow::logOut()
{
    CustomModal modal;
    modal.hideOkButton();
    modal.hideTextEdit();
    modal.setWindowTitle("Log Out");
    modal.setLabel("Are you sure you want to log out?");
    JsonHandler::writeToJson(g_OwnedSteamGamesFile, "");
    if(modal.exec() == QDialog::Accepted)
    {
        qApp -> quit();
        QProcess::startDetached(qApp -> arguments()[0], qApp -> arguments());
    }
}

void MainWindow::changePsw()
{
    changePswui = new Changepasswordui(this);
    changePswui -> open();
}

CustomTabWidget::CustomTabWidget(QWidget *parent):QTabWidget(parent)
{
    setTabBar(new CustomTabBar);
    setTabPosition(QTabWidget::West);
}

QSize CustomTabBar::tabSizeHint(int index) const
{
    QSize s = QTabBar::tabSizeHint(index);
    s.transpose();
    return s;
}

void CustomTabBar::paintEvent(QPaintEvent *){
    QStylePainter painter(this);
    QStyleOptionTab opt;

    for(int i = 0;i < count();i++)
    {
        initStyleOption(&opt,i);
        painter.drawControl(QStyle::CE_TabBarTabShape, opt);
        painter.save();
        QSize s = opt.rect.size();
        s.transpose();
        QRect r(QPoint(), s);
        r.moveCenter(opt.rect.center());
        opt.rect = r;
        QPoint c = tabRect(i).center();
        painter.translate(c);
        painter.rotate(90);
        painter.translate(-c);
        painter.drawControl(QStyle::CE_TabBarTabLabel,opt);
        painter.restore();
    }
}
