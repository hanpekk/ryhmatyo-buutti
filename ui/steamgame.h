#ifndef STEAMGAME_H
#define STEAMGAME_H

#include <QObject>
#include <QPixmap>

class SteamGame
{

public:
    SteamGame();
    QString name;
    QString bannerUrl;
    int appid;
    int playtime;
    int releaseYear;
    QList<QString> publishers;
    QList<QString> developers;
    QList<QString> genres;
    QPixmap image;
    QPixmap libraryImage;
};

#endif // STEAMGAME_H
