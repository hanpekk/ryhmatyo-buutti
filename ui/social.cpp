#include "social.h"
#include "ui_social.h"
#include "custommodal.h"


Social::Social(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Social)
{
    ui->setupUi(this);

    client = new Client(this);
    user = new User(this);
}

Social::~Social()
{
    delete ui;
}

void Social::on_addFriendButton_clicked()
{
    /*TODO Proper error checking and response to user */
    CustomModal modal;
    modal.hideOkButton();
    modal.setLabel("Your friend's user name:");
    if(modal.exec() == QDialog::Accepted)
    {
        client -> addFriend(user, modal.returnTextFromTextEdit());
        if(client->statusCode==Client::statusOk)
        {
            modal.setLabel("User added successfully to friends");
        }else
        {
            modal.setLabel("Cannot add user to friends");
        }
    }

}


void Social::on_removeFriendButton_clicked()
{
    /*TODO Proper error checking and response to user */
    CustomModal modal;
    modal.hideOkButton();
    modal.setLabel("Your friend's user name:");
    if(!(modal.exec() == QDialog::Rejected))
    {
        client -> removeFriend(user, modal.returnTextFromTextEdit());
        if(client->statusCode==Client::statusOk)
        {
            modal.setLabel("User removed successfully from friends");
        }else
        {
            modal.setLabel("Cannot remove user to friends");
        }
    }
}

