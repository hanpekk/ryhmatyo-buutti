#include "custommodal.h"
#include "ui_custommodal.h"

CustomModal::CustomModal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomModal)
{
    ui -> setupUi(this);
}

CustomModal::~CustomModal()
{
    delete ui;
}

void CustomModal::setLabel(QString text)
{
    ui -> label -> setText(text);
}

void CustomModal::hideButtonBox()
{
    ui -> buttonBox -> setVisible(false);
    ui -> pushButton -> setVisible(true);
}

void CustomModal::hideOkButton()
{
    ui -> buttonBox -> setVisible(true);
    ui -> pushButton -> setVisible(false);
}

void CustomModal::hideTextEdit()
{
    ui -> textEdit -> hide();
}

QString CustomModal::returnTextFromTextEdit()
{
    return ui -> textEdit -> text();
}

void CustomModal::enableOk()
{
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}

void CustomModal::disableOk()
{
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}
