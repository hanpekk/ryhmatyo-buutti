#include "client.h"
#include "constants.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDir>
using namespace Constants;
Client::Client(QObject *parent) : QObject(parent)
{
    user = new User(this);
    manager = new QNetworkAccessManager(this);
    ooGetOwnedGames = new OriginatingObject(this);
    ooDownloadImage = new OriginatingObject(this);

    QString homePath = QDir::homePath();

    steamIdPaths<<homePath+"/.steam/steam/config/"
                <<homePath+"/.steam/"
                <<homePath+"/.local/share/steam/"
                <<homePath+"/.local/share/Steam/"
                <<homePath+"/.steam/steam/"
                <<homePath+"/.var/app/com.valvesoftware.Steam/data/steam/"
                <<homePath+"/.steam/debian-installation/"
                <<"/usr/share/steam/"
                <<homePath+"/usr/local/share/steam/"
                <<"C:/Program Files (x86)/Steam/config/"
                <<"C:/Program Files/Steam/config/";

    image = new QPixmap();
    QObject::connect(manager, &QNetworkAccessManager::finished, this, &Client::httpReadyRead);
}

void Client::authenticate(QNetworkRequest *request)
{
    request -> setRawHeader("Authorization", "Bearer " + user -> token.toLocal8Bit());
}

void Client::httpReadyRead(QNetworkReply *reply)
{
    if(reply -> request().originatingObject() == ooDownloadImage)
    {
        image -> loadFromData(reply -> readAll());
    }
    else if( reply -> request().originatingObject() == ooGetOwnedGames)
    {
        text = reply -> readAll();
        statusCode = reply -> attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        JsonHandler::writeToJson(g_OwnedSteamGamesFile, text);
    }
    else
    {
        error = reply -> errorString();
        text = reply -> readAll();
        qDebug()<<text;
        statusCode = reply -> attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    }
}

void Client::getUsers()
{
    QNetworkRequest request(urlGetUsers);
    authenticate(&request);
    manager -> get(request);
}

void Client::addSteamGamesToDatabase(User *user)
{
    QUrl urlAddSteamGames = QUrl(serverAddress + "users/" + user -> steamId + "/addsteamgames");
    QNetworkRequest request(urlAddSteamGames);
    authenticate(&request);
    manager -> sendCustomRequest(request, "PATCH");
}

void Client::getUserSteamGames(User *user)
{
    QUrl urlGetUserSteamGames = QUrl(serverAddress + "users/" + QString::number(user -> id) + "/getusersgames");
    QNetworkRequest request(urlGetUserSteamGames);
    authenticate(&request);
    request.setOriginatingObject(ooGetOwnedGames);
    manager -> get(request);
}

void Client::addExtraSteamGameDataToDatabase(int appid)
{
    QUrl urlAddExtraSteamGameData = QUrl(serverAddress + "games/" + QString::number(appid));
    QNetworkRequest request(urlAddExtraSteamGameData);
    authenticate(&request);
    manager -> sendCustomRequest(request, "PATCH");
}

void Client::getIndividualSteamGameData(int appid)
{
    QUrl urlGetSteamGameData = QUrl(serverAddress + "games/" + QString::number(appid));
    QNetworkRequest request(urlGetSteamGameData);
    authenticate(&request);
    manager -> get(request);
}

void Client::postLogin(User *user)
{
    QNetworkRequest request(urlPostLogin);
    QJsonObject jason;
    jason.insert("email", user -> email);
    jason.insert("password", user -> password);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> post(request, QJsonDocument(jason).toJson());
}

void Client::postRegister(User *user)
{
    QNetworkRequest request(urlPostRegister);
    QJsonObject jason;
    jason.insert("email", user -> email);
    jason.insert("username", user -> username);
    jason.insert("password",user -> password);
    jason.insert("confirmpassword",user -> verify);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> post(request, QJsonDocument(jason).toJson());
}

void Client::changePassword(User *user)
{
    QString str;
    QUrl urlPatchChangePassword= QUrl(serverAddress + "users/" + str.setNum(user -> id) + "/changepassword");
    QNetworkRequest request (urlPatchChangePassword);
    QJsonObject currentPassword;
    QJsonObject newPassword;
    QJsonObject verifyNewPassword;
    QJsonArray array;

    currentPassword.insert("op","replace");
    currentPassword.insert("path","/currentpassword");
    currentPassword.insert("value",user -> password);
    array.push_back(currentPassword);

    newPassword.insert("op","replace");
    newPassword.insert("path","/newpassword");
    newPassword.insert("value",user -> newPassword);
    array.push_back(newPassword);

    verifyNewPassword.insert("op","replace");
    verifyNewPassword.insert("path","/confirmnewpassword");
    verifyNewPassword.insert("value",user -> verify);
    array.push_back(verifyNewPassword);

    authenticate(&request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> sendCustomRequest(request, "PATCH", QJsonDocument(array).toJson());
}

void Client::forgottenPassword(User *user)
{
    QNetworkRequest request(urlPostForgottenPassword);
    QJsonObject jason;
    jason.insert("email", user -> email);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> post(request, QJsonDocument(jason).toJson());
}

void Client::resetPassword(User *user)
{
    QNetworkRequest request (urlPatchResetPassword);
    QJsonObject email;
    QJsonObject code;
    QJsonObject newPassword;
    QJsonObject verifyNewPassword;
    QJsonArray array;

    email.insert("op","replace");
    email.insert("path","/email");
    email.insert("value",user -> email);
    array.push_back(email);

    code.insert("op","replace");
    code.insert("path","/resetcode");
    code.insert("value",user -> code);
    array.push_back(code);

    newPassword.insert("op","replace");
    newPassword.insert("path","/newpassword");
    newPassword.insert("value",user -> newPassword);
    array.push_back(newPassword);

    verifyNewPassword.insert("op","replace");
    verifyNewPassword.insert("path","/confirmnewpassword");
    verifyNewPassword.insert("value",user -> verify);
    array.push_back(verifyNewPassword);

    authenticate(&request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> sendCustomRequest(request, "PATCH", QJsonDocument(array).toJson());

}

void Client::getUserData()
{
    user -> id = JsonHandler::getJsonValueFromNestedJson(text,"user", "id").toInt();
    user -> token = JsonHandler::getJsonValueFromNestedJson(text,"user", "jwtToken").toString();
    user -> username = JsonHandler::getJsonValueFromNestedJson(text,"user", "username").toString();
    user -> email = JsonHandler::getJsonValueFromNestedJson(text, "user", "email").toString();
    user -> steamId = JsonHandler::getJsonValueFromNestedJson(text, "user", "steamId").toString();
}

void Client::verifyUser(User *user)
{
    QNetworkRequest request (urlPatchVerifyUser);
    QJsonObject email;
    QJsonObject code;
    QJsonArray array;

    email.insert("op", "replace");
    email.insert("path", "/email");
    email.insert("value", user -> email);
    array << email;

    code.insert("op", "replace");
    code.insert("path", "/verificationcode");
    code.insert("value", user -> code);
    array << code;

    authenticate(&request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> sendCustomRequest(request, "PATCH", QJsonDocument(array).toJson());
}

void Client::addFriend(User *user, QString friendName)
{
    QJsonObject json;

    QUrl urlAddFriend = QUrl(serverAddress + "users/" + QString::number(user -> id) + "/addfriend?name=" + friendName);
    QNetworkRequest request(urlAddFriend);
    authenticate(&request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> post(request, QJsonDocument(json).toJson());
}

void Client::getFriends()
{
 /* TODO */
}

void Client::removeFriend(User *user, QString friendName)
{
    QUrl urlRemoveFriend = QUrl(serverAddress + "users/" + QString::number(user -> id) + "/deletefriend?name=" + friendName);
    QNetworkRequest request(urlRemoveFriend);
    authenticate(&request);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> sendCustomRequest(request, "DELETE");
}

void Client::getSteamId()
{
    for (QString steamIdPath : steamIdPaths)
    {
        QFile file(steamIdPath.append(steamIdFile));
        if(file.exists()){
            file.open(QIODevice::ReadOnly | QIODevice::Text);
            QTextStream in(&file);
            QString data  =  in.readAll();
            file.close();

            QRegularExpression regex("[0-9]+");
            QRegularExpressionMatch match = regex.match(data);
            if(match.hasMatch())
            {
                user -> steamId = match.captured();
            }
            break;
        }
    }
}

void Client::downloadSteamImage(QString url)
{
    QUrl imageUrl = QUrl(url);
    QNetworkRequest request(imageUrl);
    request.setOriginatingObject(ooDownloadImage);
    manager -> get(request);
}

void Client::sendSteamId()
{
    QUrl urlPatchSendSteamId = QUrl(serverAddress + "users/" + QString::number(user -> id) + "/addsteamid");
    QNetworkRequest request(urlPatchSendSteamId);
    QJsonObject steamId;
    QJsonArray array;

    steamId.insert("op", "replace");
    steamId.insert("path", "/steamid");
    steamId.insert("value", user -> steamId);
    array << steamId;

    authenticate(&request);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager -> sendCustomRequest(request, "PATCH", QJsonDocument(array).toJson());
}

OriginatingObject::OriginatingObject(QObject *parent)
{}
