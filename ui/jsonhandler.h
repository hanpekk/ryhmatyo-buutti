#ifndef JSONHANDLER_H
#define JSONHANDLER_H

#include <QObject>
#include <QFile>

class JsonHandler : public QObject
{
    Q_OBJECT
public:

    explicit JsonHandler(QObject *parent = nullptr);
    static QString readJson(QString fileName);
    static void writeToJson(QString fileName, QString contents);
    static void appendToJson(QString fileName, QString contents);
    static QVariant getJsonValueFromString(QString jsonData, QString key);
    static QVariant getJsonValueFromNestedJson(QString jsonData,QString key1, QString key2);
    static QVariant getJsonValueFromSteamPlayerSummary(QString jsonData, QString key);
    static QVariant getJsonValueFromSteamOwnedGames(QString jsonData, QString key);
};

#endif // JSONHANDLER_H
