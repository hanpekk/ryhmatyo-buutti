#include "steamui.h"
#include "jsonhandler.h"
#include "constants.h"
#include <QJsonArray>
#include <QAction>
#include <QJsonObject>
#include <QJsonDocument>

using namespace Constants;

SteamUi::SteamUi(QWidget *parent) :
    GameTab(parent)
{}

SteamUi::~SteamUi()
{}

void SteamUi::findSteamImageFolderPath()
{
    for(QString path: steamImageFolderPaths)
    {
        QFile folder(path);
        if(folder.exists()){
            steamImageFolderPath = path;
            break;
        }
    }
}

void SteamUi::setupTable()
{
    findSteamImageFolderPath();
    populateGames();
    populateItemTable();
    setGameTable();
}

void SteamUi::populateGames()
{
    QFile file(g_OwnedSteamGamesFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString OwnedSteamGamesData = file.readAll();
    file.close();

    QJsonDocument steamGamesJson = QJsonDocument::fromJson(OwnedSteamGamesData.toUtf8());
    QJsonObject steamGamesJsonObject = steamGamesJson.object();
    QJsonValue games = steamGamesJsonObject["games"];
    QJsonArray gamesJsonArray = games.toArray();


    for (int i = 0 ; i < gamesJsonArray.size() ; i++)
    {
        QList<QString> developersList;
        QList<QString> publishersList;
        QList<QString> genresList;
        QString name = gamesJsonArray.at(i)["game"]["name"].toString();
        int appid = gamesJsonArray.at(i)["game"]["steamId"].toInt();
        int playtime = gamesJsonArray.at(i)["playedHours"].toInt();
        int releaseyear = gamesJsonArray.at(i)["game"]["releaseYear"].toInt();

        QJsonArray developersArray = gamesJsonArray.at(i)["game"]["developers"].toArray();

        if(!(developersArray.isEmpty())){
        for (int j = 0 ; j < developersArray.size() ; j++ )
        {
            developersList.emplace_back(developersArray.at(j)["developers"].toString());
        }}

        QJsonArray publishersJsonArray = gamesJsonArray.at(i)["game"]["publishers"].toArray();
        if(!(publishersJsonArray.isEmpty())){
        for ( int x = 0 ; x < publishersJsonArray.size() ; x++ )
        {
            publishersList.emplace_back(publishersJsonArray.at(x)["name"].toString());
        }}

        QJsonArray genresJsonArray = gamesJsonArray.at(i)["game"]["genres"].toArray();
        if(!(genresJsonArray.isEmpty())){
        for (int y = 0 ; y < genresJsonArray.size() ; y++)
        {
            genresList.emplace_back(genresJsonArray.at(y)["description"].toString());
        }}

        steamGames << createSteamGame(name, appid, playtime, releaseyear, publishersList, developersList, genresList);
    }

}

void SteamUi::on_updateTable_clicked()
{
    clearGameTable();
    this->setupTable();
}

SteamGame* SteamUi::createSteamGame(QString name, int appid, int playtime, int releaseYear, QList<QString> publishers , QList<QString> developers, QList<QString> genres )
{
    SteamGame *game = new SteamGame();
    game -> name = name;
    game -> appid = appid;
    game -> playtime = playtime;
    game -> image.load(steamImageFolderPath + QString::number(game -> appid) + steamBannerImagePathEnd);
    game -> libraryImage.load(steamImageFolderPath + QString::number(game -> appid) + steamLibraryImagePathEnd);
    game -> releaseYear = releaseYear;
    game -> publishers = publishers;
    game -> developers = developers;
    game -> genres = genres;

    return game;
}
