#ifndef USERUI_H
#define USERUI_H

#include <QWidget>
#include <QFileSystemWatcher>
#include "custommodal.h"
#include "jsonhandler.h"
#include "client.h"
#include "user.h"

namespace Ui {
class Userui;
}

class UserUi : public QWidget
{
    Q_OBJECT

public:
    explicit UserUi(QWidget *parent = nullptr);
    ~UserUi();

    QFileSystemWatcher *watcher;
    QPixmap *avatar;
    Client *client;
    User *user;

public slots:
    void on_buttonLogout_clicked();
    void on_buttonChangePsw_clicked();
    void on_buttonSyncSteam_clicked();
    void changeLoggedInAsName();
    void prepareSteamUser();
    void showSteamAvatar();

private:
    const QString steamUserAvatar = "avatarmedium";
    const QString steamNickname = "personaname";
    Ui::Userui *ui;

signals:
    void logOut();
    void changePsw();
    void steamSync();
};

#endif // USERUI_H
