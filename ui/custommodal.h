#ifndef CUSTOMMODAL_H
#define CUSTOMMODAL_H

#include <QDialog>

namespace Ui {
class CustomModal;
}

class CustomModal : public QDialog
{
    Q_OBJECT

public:
    explicit CustomModal(QWidget *parent = nullptr);
    ~CustomModal();

    void setLabel(QString text);
    void hideButtonBox();
    void hideOkButton();
    void hideTextEdit();
    QString returnTextFromTextEdit();
    void enableOk();
    void disableOk();



    Ui::CustomModal *ui;
private:
};

#endif // CUSTOMMODAL_H
