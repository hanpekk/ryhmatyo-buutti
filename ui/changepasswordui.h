#ifndef CHANGEPASSWORDUI_H
#define CHANGEPASSWORDUI_H

#include <QDialog>
#include "client.h"

namespace Ui {
class Changepasswordui;
}

class Changepasswordui : public QDialog
{
    Q_OBJECT

    Client *client;

public:
    explicit Changepasswordui(QWidget *parent = nullptr);
    ~Changepasswordui();

private slots:
    void on_buttonConfirm_clicked();
   bool validateOldPassword();
   bool validateNewPassword();
   bool validate();
   void result();
   void disableAll();
   void enableAll();
   void Close();

private:
    Ui::Changepasswordui *ui;
};

#endif // CHANGEPASSWORDUI_H
