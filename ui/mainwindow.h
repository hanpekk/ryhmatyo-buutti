#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QTabBar>
#include <QTabWidget>
#include <QStylePainter>
#include <QStyleOption>
#include "allgames.h"
#include "jsonhandler.h"
#include "steamui.h"
#include "login.h"
#include "register.h"
#include "user.h"
#include "userui.h"
#include "custommodal.h"
#include "changepasswordui.h"
#include "social.h"

class CustomTabWidget : public QTabWidget
{
public:
    CustomTabWidget(QWidget *parent);
};

class CustomTabBar: public QTabBar{
public:
    QSize tabSizeHint(int index) const;
protected:
    void paintEvent(QPaintEvent * /*event*/);
};

QT_BEGIN_NAMESPACE

class function;
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

   QVector<GameTab*> collections;

public slots:


private:
    Ui::MainWindow *ui;

    QWidget *mainWidget;
    QVBoxLayout *vbox ;
    QTabWidget *viewsTabWidget;
    QWidget *gamesWidget;
    QWidget *storeWidget;

    Login *loginWindow;
    User *user;
    CustomModal *modal;
    Changepasswordui *changePswui;
    CustomTabWidget *verticalTabWidget;

    AllGames *allGames;
    SteamUi *steamUi;
    UserUi *userUi;
    Social *socialUi;

    QWidget* createGamesTab();
    QWidget* createStoreTab();
    Social* createSocialTab();
    UserUi *createUserTab();
    SteamUi *createSteamUi();
    AllGames *createAllGamesUi();

    void logOut();
    void changePsw();

};


#endif // MAINWINDOW_H
