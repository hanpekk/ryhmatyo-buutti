#include <QTimer>
#include "constants.h"
#include "login.h"
#include "register.h"
#include "ui_register.h"

using namespace Constants;

Register::Register(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Register)
{
    ui -> setupUi(this);
    client = new Client(this);
    this -> setStyleSheet("QLineEdit#textEmail { border: 1px solid red }"
                        "QLineEdit#textUsername{ border: 1px solid red }"
                        "QLineEdit#textPassword { border: 1px solid red }"
                        "QLineEdit#textVerify { border: 1px solid red }");

    QObject::connect(client -> manager, &QNetworkAccessManager::finished, this, &Register::result);
    QObject::connect(ui -> textEmail,&QLineEdit::textEdited,this, &Register::validateEmail);
    QObject::connect(ui -> textUsername,&QLineEdit::textEdited, this, &Register::validateUsername);
    QObject::connect(ui -> textPassword,&QLineEdit::textEdited,this, &Register::validatePassword);
    QObject::connect(ui -> textVerify,&QLineEdit::textEdited,this, &Register::validateVerify);
    QObject::connect(ui -> textUsername,&QLineEdit::textEdited,this, &Register::validate);
    QObject::connect(ui -> textEmail,&QLineEdit::textEdited,this, &Register::validate);
    QObject::connect(ui -> textPassword,&QLineEdit::textEdited,this, &Register::validate);
    QObject::connect(ui -> textVerify,&QLineEdit::textEdited,this, &Register::validate);
    ui -> buttonCreate -> setEnabled(false);

}

Register::~Register()
{
    delete ui;
}

void Register::on_buttonCreate_clicked()
{
    disableAll();
    ui -> status -> clear();
    client -> postRegister(client -> user);
}

void Register::validate()
{
    ui -> buttonCreate -> setEnabled(false);
    if(validateEmail()
            &&validatePassword()
            &&validateUsername()
            &&validateVerify())
    {
        ui -> buttonCreate -> setEnabled(true);
    }
}

bool Register::validateEmail()
{
    bool valid = false;
    client -> user -> email =  ui -> textEmail -> text();
    QRegularExpression regex("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
    if(regex.match(client -> user -> email).hasMatch())
    {
        valid = true;
        ui -> textEmail -> setStyleSheet("QLineEdit#textEmail { border: 1px solid green }");
    }else{
        ui -> textEmail -> setStyleSheet("QLineEdit#textEmail { border: 1px solid red }");
    }
    return valid;
}

bool Register::validateUsername()
{
    bool valid = false;
    client -> user -> username = ui -> textUsername -> text();
    if(client -> user -> username.length()>=g_MinUserNameLen
            && client -> user -> username.length()<=g_MaxUserNameLen){
        valid = true;
        ui -> textUsername -> setStyleSheet("QLineEdit#textUsername { border: 1px solid green }");
    }else{
        ui -> textUsername -> setStyleSheet("QLineEdit#textUsername{ border: 1px solid red }");
    }
    return valid;
}

bool Register::validatePassword()
{
    bool valid = false;
    client -> user -> password = ui -> textPassword -> text();
    if(client -> user -> password.length()>=g_MinPasswordLen){
        valid = true;
        ui -> textPassword -> setStyleSheet("QLineEdit#textPassword { border: 1px solid green }");
    }else{
        ui -> textPassword -> setStyleSheet("QLineEdit#textPassword{ border: 1px solid red }");
    }
    return valid;
}

bool Register::validateVerify()
{
    bool valid = false;
    client -> user -> verify = ui -> textVerify -> text();
    if(client -> user -> password == client -> user -> verify){
        valid = true;
        ui -> textVerify -> setStyleSheet("QLineEdit#textVerify { border: 1px solid green }");
    }else{
        ui -> textVerify -> setStyleSheet("QLineEdit#textVerify{ border: 1px solid red }");
    }
    return valid;
}

void Register::result()
{
    switch (client -> statusCode)
    {
    case(Client::noConnection):
        ui -> status -> setText("Could not establish connection to server");
        enableAll();
        break;
    case(Client::badRequest):
        if(client -> text.contains("username"))
        {
            ui -> status -> setText("Username is already taken");
            enableAll();
        }
        if(client -> text.contains("email"))
        {
            ui -> status -> setText("Email is already in use");
            enableAll();
        }
        break;
    case(Client::statusOk):
        ui -> status -> setText("Account created succesfully! A verification code has been sent to your email address, it has to be entered on first login.");
        QTimer::singleShot(3000, this,  SLOT(on_buttonBacktoLogin_clicked()));
        break;
    }
}

void Register::on_buttonBacktoLogin_clicked()
{
    this -> close();
}

void Register::disableAll()
{
    ui -> textEmail -> setEnabled(false);
    ui -> textUsername -> setEnabled(false);
    ui -> textPassword -> setEnabled(false);
    ui -> textVerify -> setEnabled(false);
    ui -> buttonCreate -> setEnabled(false);
    ui -> buttonBacktoLogin -> setEnabled(false);
}

void Register::enableAll()
{
    ui -> textEmail -> setEnabled(true);
    ui -> textUsername -> setEnabled(true);
    ui -> textPassword -> setEnabled(true);
    ui -> textVerify -> setEnabled(true);
    ui -> buttonCreate -> setEnabled(true);
    ui -> buttonBacktoLogin -> setEnabled(true);
}

void Register::clearAll()
{
    ui -> textEmail -> clear();
    ui -> textUsername -> clear();
    ui -> textPassword -> clear();
    ui -> textVerify -> clear();
    ui -> status -> clear();
}
