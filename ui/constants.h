#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <QString>
 namespace Constants{

    const QString g_TempFile{"temp/temp.json"};
    const QString g_SteamUserFile{"temp/steam_user.json"};
    const QString g_OwnedSteamGamesFile{"temp/owned_steam_games.json"};
    const QString g_AllSteamGamesFile {"temp/all_steam_games.json"};

    const int g_MinPasswordLen  = 8;
    const int g_MinUserNameLen = 6;
    const int g_MaxUserNameLen = 20;

}
#endif // CONSTANTS_H
