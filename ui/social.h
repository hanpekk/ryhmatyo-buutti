#ifndef SOCIAL_H
#define SOCIAL_H

#include <QDialog>
#include "client.h"

namespace Ui {
class Social;
}

class Social : public QDialog
{
    Q_OBJECT

public:
    explicit Social(QWidget *parent = nullptr);
    ~Social();

    Client *client;
    User *user;

private slots:
    void on_addFriendButton_clicked();

    void on_removeFriendButton_clicked();

private:
    Ui::Social *ui;
};

#endif // SOCIAL_H
