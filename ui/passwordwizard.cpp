#include "passwordwizard.h"
#include <QPushButton>
#include "constants.h"

using namespace Constants;

PasswordWizard::PasswordWizard(QWidget *parent) : QWizard(parent)
{
    setPage(Page_First, new FirstPage);
    setPage(Page_Last, new SecondPage);
    setStartId(Page_First);
    setWindowTitle("Password Wizard");
    setMinimumSize(700 ,500);
    setMaximumSize(700, 500);
}

FirstPage::FirstPage(QWidget *parent)
    : QWizardPage(parent)
{

    datasender = new DataSender(this);
    setWindowTitle("Password Wizard");

    label = new QLabel("<b>Welcome to the Password Wizard. To start resetting your password, type in your email address. A verification code will be sent there</b>", this);
    label -> setWordWrap(true);

    email = new QLineEdit(this);
    email -> setMinimumSize(440, 31);
    email -> setPlaceholderText("Your email address");

    layout = new QVBoxLayout;

    send = new QPushButton("Send", this);
    send -> setEnabled(false);
    send -> setMinimumSize(100, 36);
    send -> setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);


    status = new QLabel(this);
    status -> setWordWrap(true);

    layout -> addWidget(label);
    layout -> addWidget(email);
    layout -> addWidget(send, 0, Qt::AlignHCenter);
    layout -> addWidget(status);

    this -> setLayout(layout);

    QObject::connect(email, &QLineEdit::textChanged, this, &FirstPage::setEmail);
    QObject::connect(send, &QPushButton::clicked, this, &FirstPage::disableAll);
    QObject::connect(send, &QPushButton::clicked, datasender, &DataSender::sendEmail);
    QObject::connect(datasender -> client -> manager, &QNetworkAccessManager::finished, this, &FirstPage::result);
}

bool FirstPage::isComplete() const
{
    return emailSent;
}

void FirstPage::disableAll()
{
    email -> setEnabled(false);
    send -> setEnabled(false);
}

void FirstPage::setEmail()
{
    QRegularExpression regex("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
    if(regex.match(email -> text()).hasMatch()){
        send -> setEnabled(true);
        datasender -> client -> user -> email = email -> text();
    }
    else
    {
        send -> setEnabled(false);
    }
}

void FirstPage::result()
{
    switch (datasender -> client -> statusCode) {
    case (Client::noConnection):
        email -> setEnabled(true);
        send -> setEnabled(true);
        status -> setText(datasender -> client -> STATUS_MSG_NO_CONNECTION);
        break;
    case(Client::badRequest):
        email -> setEnabled(true);
        send -> setEnabled(true);
        status -> setText(datasender -> client -> STATUS_MSG_INVALID_EMAIL);
        break;
    case(Client::statusOk):
        status -> setText(datasender -> client -> STATUS_MSG_CODE_SUCCESSFUL);
        emailSent = true;
        emit completeChanged();
        break;
    }
}

SecondPage::SecondPage(QWidget *parent)
    : QWizardPage(parent)
{
    setFinalPage(true);
    datasender  = new DataSender(this);
    setWindowTitle("Password Wizard");

    label = new QLabel("<b> Type in the code that was sent to your email address and your new password</b>", this);
    label -> setWordWrap(true);

    labelCode = new QLabel("Code: ", this);
    code = new QLineEdit(this);
    code -> setPlaceholderText("Verification code");
    code -> setMinimumSize(440, 31);

    labelPassword = new QLabel("New password:" , this);
    labelPassword -> setWordWrap(true);
    password = new QLineEdit(this);
    password -> setPlaceholderText("New password");
    password -> setEchoMode(QLineEdit::Password);
    password -> setMinimumSize(440, 31);

    labelVerify = new QLabel("New password again: ", this);
    verify = new QLineEdit(this);
    verify -> setPlaceholderText("Password again");
    verify -> setEchoMode(QLineEdit::Password);
    verify -> setMinimumSize(440, 31);


    layout = new QVBoxLayout(this);

    send = new QPushButton("Send", this);
    send -> setEnabled(false);
    send -> setMinimumSize(100, 36);
    send -> setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    status = new QLabel(this);
    status -> setWordWrap(true);
    status -> setMinimumHeight(100);
    status -> setMinimumWidth(100);

    layout -> addWidget(label);
    layout -> addWidget(labelCode);
    layout -> addWidget(code);
    layout -> addWidget(labelPassword);
    layout -> addWidget(password);
    layout -> addWidget(labelVerify);
    layout -> addWidget(verify);
    layout -> addWidget(send,0, Qt::AlignHCenter);
    layout -> addWidget(status);
    this -> setLayout(layout);

    QObject::connect(code, &QLineEdit::textChanged, this, &SecondPage::verifyCodeAndPassword);
    QObject::connect(password, &QLineEdit::textChanged, this, &SecondPage::verifyCodeAndPassword);
    QObject::connect(verify, &QLineEdit::textChanged, this, &SecondPage::verifyCodeAndPassword);
    QObject::connect(send, &QPushButton::clicked, datasender, &DataSender::sendCodeAndPassword);
    QObject::connect(send, &QPushButton::clicked, this, &SecondPage::disableAll);
    QObject::connect(datasender -> client -> manager, &QNetworkAccessManager::finished, this, &SecondPage::result);
}

bool SecondPage::isComplete() const
{
    return codeSent;
}

void SecondPage::verifyCodeAndPassword()
{
    if(code -> text().size() >= g_MinPasswordLen
            && password -> text().size() >= g_MinPasswordLen
            && verify -> text() == password -> text())
    {
        send -> setEnabled(true);
        datasender -> client -> user -> code = code -> text();
        datasender -> client -> user -> newPassword = password -> text();
        datasender -> client -> user -> verify = verify -> text();
    }
    else
    {
        send -> setEnabled(false);
        status -> setText("Both your verification code and your new password should be at least 8 characters long. \n The passwords also need to match.");
    }
}

void SecondPage::disableAll()
{
    code -> setEnabled(false);
    password -> setEnabled(false);
    verify -> setEnabled(false);
    send -> setEnabled(false);
}

void SecondPage::enableAll()
{
    code -> setEnabled(true);
    password -> setEnabled(true);
    verify -> setEnabled(true);
    send -> setEnabled(true);
}

void SecondPage::result()
{
    if(datasender -> client -> statusCode == datasender -> client -> noConnection){
        enableAll();
        status -> setText(datasender -> client -> STATUS_MSG_NO_CONNECTION);
    }
    if(datasender -> client -> statusCode == datasender -> client -> badRequest){
        enableAll();
        status -> setText(datasender -> client -> STATUS_MSG_INVALID_CODE);
    }
    if(datasender -> client -> statusCode == datasender -> client -> statusOk){
        status -> setText(datasender -> client -> STATUS_MSG_PASSWORD_CHANGE_SUCCESSFUL);
        codeSent = true;
        emit completeChanged();
        status -> setText("<b>Your password has been successfully reset! Login to your account with your new password.</b>");
    }
}

DataSender::DataSender(QObject *parent) : QObject(parent)
{
    client = new Client(this);
}

void DataSender::sendEmail()
{
    client -> forgottenPassword(client -> user);
}

void DataSender::sendCodeAndPassword()
{
    client -> resetPassword(client -> user);
}
