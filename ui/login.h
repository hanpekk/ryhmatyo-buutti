#ifndef LOGIN_H
#define LOGIN_H
#include <QObject>
#include <QWidget>
#include <QDialog>
#include "client.h"
#include "register.h"
#include "custommodal.h"

namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = nullptr);
    ~Login();

    Client *client;
    Register *registerW;
    CustomModal *verifyModal;

public slots:
    void on_buttonCreateAccount_clicked();
    void on_buttonLogin_clicked();
    void result();
    void on_buttonCancel_clicked();
    bool validateEmail();
    bool validatePassword();
    void validate();
    void Accept();
    void disableAll();
    void enableAll();
    void validateVerify();

signals:
    void loggedIn();

private slots:
    void on_buttonForgotPassword_clicked();

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
